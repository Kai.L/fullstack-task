import express, { Express } from 'express'
import bodyParser from 'body-parser'
import config from './config'
import booksRouter from './routers/booksRouter'
import seeder from './data/seeder'

const app: Express = express()

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/books', booksRouter)

app.use((req, res) => {
    if (!req.route) {
        res.status(404).json("Not found")
    }
});

const startServer = async () => {
    await seeder()
    app.listen(config.port, () => {
        console.log(`Server is listening at port ${config.port}`)
    });
}

startServer()