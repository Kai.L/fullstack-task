import { Request, Response } from "express"
import Book from "../data/models/Book"
import responses from "../utils/responses"

export const getBooksController = (_req: Request, res: Response) => {
    Book.findAll()
        .then((result) => {
            responses.okResponse(res, result)
        })
        .catch((err) => {
            console.error(err)
            responses.serverErrorResponse(res)
        })
}

export const createBookController = (req: Request, res: Response) => {
    const { title, author, description } = req.body

    if (!title || !author || !description) {
        responses.badRequestResponse(res, "Bad Request")
        return
    }

    Book.create({
        title,
        author,
        description
    })
        .then((result) => {
            responses.createdResponse(res, result)
        })
        .catch((err) => {
            console.error(err)
            responses.serverErrorResponse(res)
        })
}

export const updateBookController = (req: Request, res: Response) => {
    const { bookId } = req.params
    const { title, author, description } = req.body

    if (!bookId || !title || !author || !description) {
        responses.badRequestResponse(res, "Bad Request")
        return
    }

    Book.update(
        {
            title,
            author,
            description
        },
        {
            where: {
                id: bookId
            },
            returning: true,
        }
    )
        .then((result) => {
            result[0] ?
                responses.okResponse(res, result[1][0])
                :
                responses.notFoundResponse(res, "Not found")
        })
        .catch((err) => {
            console.error(err)
            responses.serverErrorResponse(res)
        })
}

export const deleteBookController = (req: Request, res: Response) => {
    const { bookId } = req.params

    Book.destroy({
        where: {
            id: bookId
        },
    })
        .then((result) => {
            result ?
                responses.okResponse(res, "Deleted successfully.")
                :
                responses.notFoundResponse(res, "Not found")
        })
        .catch((err) => {
            console.error(err)
            responses.serverErrorResponse(res)
        })
}

export default {
    getBooksController,
    createBookController,
    updateBookController,
    deleteBookController
}