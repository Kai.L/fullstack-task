import { Router } from "express";
import { createBookController, deleteBookController, getBooksController, updateBookController } from "../controllers/booksController";

const booksRouter = Router()

booksRouter.get('/all', getBooksController)
booksRouter.post('/', createBookController)
booksRouter.put('/:bookId', updateBookController)
booksRouter.delete('/:bookId', deleteBookController)

export default booksRouter