import pg from 'pg'
import db from './db'
import Book from './models/Book'
import config from '../config'

const createNewDB = async () => {

    const dbName = config.dbName,
        username = config.dbUser,
        password = config.dbPassword,
        host = config.dbHost

    let createdNewDB = false
    const conStringPri = 'postgres://' + username + ':' + password + '@' + host + '/postgres';

    const client = new pg.Client(conStringPri)

    try {
        await client.connect()
        const result = await client.query(
            `select exists(SELECT datname FROM pg_catalog.pg_database WHERE datname = '${dbName}');`
        )

        if (!result.rows[0].exists) {
            await client.query('CREATE DATABASE ' + dbName)
            createdNewDB = true
        }

    } catch (error) {
        console.log(error)
    }

    return createdNewDB
}

const seeder = async () => {
    const createdNewDB = await createNewDB()

    if (createdNewDB) {
        await db.sync()
            .catch(error => console.log(error))

        await Book.bulkCreate([
            {
                title: "In Search of Lost Time",
                author: "Marcel Proust",
                description: `Swanns Way, 
                the first part of A la recherche de temps perdu, 
                Marcel Proust's seven-part cycle, 
                was published in 1913. In it, 
                Proust introduces the themes that run through the entire work. 
                The narrator recalls his childhood, 
                aided by the famous madeleine; 
                and describes M. Swann's passion for Odette. 
                The work is incomparable. 
                Edmund Wilson said 
                "[Proust] has supplied for the first time in literature 
                an equivalent in the full scale for the new theory of modern physics."`
            },
            {
                title: "Ulysses",
                author: "James Joyce",
                description: `Ulysses chronicles the passage of Leopold Bloom through Dublin during an ordinary day, 
                June 16, 1904. The title parallels and alludes to Odysseus (Latinised into Ulysses), 
                the hero of Homer's Odyssey (e.g., the correspondences between Leopold Bloom and Odysseus, 
                Molly Bloom and Penelope, and Stephen Dedalus and Telemachus). 
                Joyce fans worldwide now celebrate June 16 as Bloomsday.`
            },
            {
                title: "Don Quixote",
                author: "Miguel de Cervantes",
                description: `Alonso Quixano, 
                a retired country gentleman in his fifties, 
                lives in an unnamed section of La Mancha with his niece and a housekeeper. 
                He has become obsessed with books of chivalry, 
                and believes their every word to be true, despite the fact 
                that many of the events in them are clearly impossible. 
                Quixano eventually appears to other people to have lost his mind
                from little sleep and food and because of so much reading.`
            },
        ]).catch(error => console.log(error))
    }
}

export default seeder