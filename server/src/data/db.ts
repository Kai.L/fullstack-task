import { Sequelize } from "sequelize"
import config from '../config'

const db = new Sequelize(`${config.dbConnection}`, {
    logging: false,
})

export default db
