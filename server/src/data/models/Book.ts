import db from '../db'
import { DataTypes } from 'sequelize'

const Book = db.define('Book', {
    title: DataTypes.TEXT,
    author: DataTypes.TEXT,
    description: DataTypes.TEXT,
},
    { timestamps: false }
)

export default Book