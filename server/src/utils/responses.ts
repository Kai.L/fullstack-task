import { Response } from "express"

export const okResponse = (res: Response, data: any) => {
    res.status(200).json(data)
}

export const createdResponse = (res: Response, data: any) => {
    res.status(201).json(data)
}

export const badRequestResponse = (res: Response, data: any) => {
    res.status(400).send(data)
}

export const notFoundResponse = (res: Response, data: any) => {
    res.status(404).json(data)
}

export const serverErrorResponse = (res: Response) => {
    res.status(500).json("OH NOES!!! SOMETHING BAD HAPPENED!!!")
}

export default {
    okResponse,
    createdResponse,
    badRequestResponse,
    notFoundResponse,
    serverErrorResponse
}
