import * as dotenv from 'dotenv'

dotenv.config();

export default {
    dbConnection: process.env.DB_CONNECTION,
    port: process.env.PORT,
    dbName: process.env.DB_NAME,
    dbUser: process.env.DB_USER,
    dbHost: process.env.DB_HOST,
    dbPassword: process.env.DB_PASSWORD
};