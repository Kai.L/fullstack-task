# fullstack-task

## Installation
1. If you haven't already, install the following software:
- PostgreSQL https://www.postgresql.org/
- Node.js https://nodejs.org/en/

2. Clone the project.

3. In the server file, create a file named .env and add the following environment variables:

PORT=3001
DB_CONNECTION="postgres://postgres:password@127.0.0.1:5432/fullstack_task"
DB_USER="postgres"
DB_HOST="localhost"
DB_PASSWORD="password"
DB_NAME="fullstack_task"

4. In command line, go to the project's server file and run the following command:
npm run dev-install-start-server-client

This should install all the required npm packages and start both the server and the client.
