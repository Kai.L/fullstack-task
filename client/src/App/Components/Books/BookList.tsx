import { v4 as uuidv4 } from 'uuid'
import { Book } from '../../../lib/types'
import BookItem from './BookItem'
import { Dispatch, SetStateAction } from 'react'
import styles from './BookList.module.css'

interface Props {
    selectedBook: Book;
    setSelectedBook: Dispatch<SetStateAction<Book>>;
    setSelectedBookEmpty: () => void;
    books: Book[]
}

const BookList = ({
    selectedBook,
    setSelectedBook,
    setSelectedBookEmpty,
    books
}: Props) => {

    const getBooks = (books: Book[]) => {
        return books.map((book) =>
            <BookItem
                key={uuidv4()}
                bookInfo={{
                    id: book.id,
                    title: book.title,
                    author: book.author,
                    description: book.description
                }}
                selectedBook={selectedBook}
                setSelectedBook={setSelectedBook}
                setSelectedBookEmpty={() => { setSelectedBookEmpty() }}
            />
        )
    }

    return (
        <div className={styles.main}>
            {getBooks(books)}
        </div>
    )
}

export default BookList