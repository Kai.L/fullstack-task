import styles from './BookForm.module.css'
import axios from 'axios'
import { useState, useEffect } from 'react';
import TextInput from '../../../lib/CommonComponents/TextInput';
import { Book } from '../../../lib/types'
import Button from '../../../lib/CommonComponents/Button';

interface Props {
    selectedBook: Book;
    setSelectedBookEmpty: () => void;
    updateBooksList: () => void
}

const BookForm = ({
    selectedBook,
    setSelectedBookEmpty,
    updateBooksList
}: Props) => {

    const [bookInfo, setBookInfo] = useState<Book>(selectedBook)

    useEffect(() => {
        setBookInfo(selectedBook)
    }, [selectedBook])

    const createBook = ({
        title,
        author,
        description,
        setSelectedBookEmpty,
        updateBooksList
    }: Omit<Book, "id"> & {
        setSelectedBookEmpty: () => void;
        updateBooksList: () => void;
    }) => {

        if (!title || !author || !description) {
            alert("Did you fill all fields?")
            return
        }

        axios.post("/books", {
            title,
            author,
            description
        })
            .then((_response) => {
                setSelectedBookEmpty()
                updateBooksList()
            })
            .catch((error) => {
                if (error.response.status === 400) {
                    alert("Did you fill all the fields?")
                } else {
                    alert("Oops! Something went wrong. Try again later.")
                }
            })
    }

    const updateBook = ({
        id,
        title,
        author,
        description,
        setSelectedBookEmpty,
        updateBooksList
    }: Book & {
        setSelectedBookEmpty: () => void;
        updateBooksList: () => void;
    }) => {

        if (!id) {
            alert("Book was not found. Refresh the page or try again later.")
            return
        } else if (!title || !author || !description) {
            alert("Did you fill all the fields?")
            return
        }

        axios.put(`/books/${id}`, {
            title,
            author,
            description
        })
            .then((response) => {
                console.log(response)
                setSelectedBookEmpty()
                updateBooksList()
            })
            .catch((error) => {
                if (error.response.status === 400) {
                    alert("Did you fill all the fields?")
                } else if (error.response.status === 404) {
                    alert("Resource was not found. Refresh the page or try again later.")
                }
                else {
                    alert("Oops! Something went wrong. Refresh the page or try again later.")
                }
            })
    }

    const deleteBook = (
        id: number,
        setSelectedBookEmpty: () => void,
        updateBooksList: () => void
    ) => {

        if (!id) {
            alert("Book was not found. Refresh the page or try again later.")
            return
        }

        axios.delete(`/books/${id}`)
            .then((response) => {
                console.log(response)
                setSelectedBookEmpty()
                updateBooksList()
            })
            .catch((error) => {

                if (error.response.status === 404) {
                    alert("Resource was not found. Refresh the page or try again later.")
                } else {
                    alert("Oops! Something went wrong. Refresh the page or try again later.")
                }
            })
    }

    return (
        <div className={styles.main}>
            <div className={styles.header}>
                {bookInfo.id ? "Update book" : "Add book"}
            </div>
            <div className={styles.formContainer}>
                <div className={styles.inputsContainer}>
                    <TextInput
                        value={bookInfo.title as string}
                        setValue={(title: string) => {
                            setBookInfo((prevState: any) => {
                                return {
                                    ...prevState,
                                    title: title
                                }
                            })
                        }}
                        placeHolder={"..."}
                        width={"calc(100% - 50px)"}
                        padding={"10px"}
                        margin={"15px"}
                        label={"Title"}
                    />
                    <TextInput
                        value={bookInfo.author}
                        setValue={(author: string) => {
                            setBookInfo((prevState: any) => {
                                return {
                                    ...prevState,
                                    author: author
                                }
                            })
                        }}
                        placeHolder={"..."}
                        width={"calc(100% - 50px)"}
                        padding={"10px"}
                        margin={"15px"}
                        label={"Author"}
                    />
                    <TextInput
                        value={bookInfo.description}
                        setValue={(description: string) => {
                            setBookInfo((prevState: any) => {
                                return {
                                    ...prevState,
                                    description: description
                                }
                            })
                        }}
                        placeHolder={"..."}
                        width={"calc(100% - 50px)"}
                        padding={"10px"}
                        margin={"15px"}
                        area={true}
                        label={"Description"}
                        height={"80px"}
                    />
                </div>
                <div className={styles.buttonsContainer}>
                    <Button
                        onClickFunction={() => {
                            createBook({
                                title: bookInfo.title,
                                author: bookInfo.author,
                                description: bookInfo.description,
                                setSelectedBookEmpty,
                                updateBooksList
                            })
                        }}
                        label={"Save New"}
                        disabled={!!bookInfo.id}
                        color={"rgb(53, 123, 252)"}
                    />
                    <Button
                        onClickFunction={() => {
                            updateBook({
                                id: bookInfo.id,
                                title: bookInfo.title,
                                author: bookInfo.author,
                                description: bookInfo.description,
                                setSelectedBookEmpty,
                                updateBooksList
                            })
                        }}
                        label={"Save"}
                        disabled={!bookInfo.id}
                        color={"rgb(53, 123, 252)"}
                    />
                    <Button
                        onClickFunction={() => {
                            deleteBook(
                                bookInfo.id as number,
                                setSelectedBookEmpty,
                                updateBooksList
                            )
                        }}
                        label={"Delete"}
                        disabled={!bookInfo.id}
                        color={"rgb(255, 55, 75)"}
                    />
                </div>
            </div>
        </div>
    )
}

export default BookForm