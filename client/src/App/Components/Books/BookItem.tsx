import { Dispatch, SetStateAction } from 'react';
import styles from './BookItem.module.css'
import { Book } from '../../../lib/types'

interface Props {
    bookInfo: Book;
    selectedBook: Book;
    setSelectedBook: Dispatch<SetStateAction<Book>>;
    setSelectedBookEmpty: () => void;
}

const BookItem = ({
    bookInfo,
    selectedBook,
    setSelectedBook,
    setSelectedBookEmpty
}: Props) => {

    return (
        <div className={styles.main}
            is-selected={(selectedBook?.id === bookInfo.id).toString()}
            onClick={() => {
                selectedBook?.id === bookInfo.id ?
                    setSelectedBookEmpty()
                    :
                    setSelectedBook(bookInfo)
            }}
        >

            <div className={styles.bookInfo}>
                <p className={styles.title}>{`Title: ${bookInfo.title}`}</p>
                <p className={styles.author}>{`Author: ${bookInfo.author}`}</p>
            </div>
        </div>
    )
}

export default BookItem