import styles from './Books.module.css'
import axios from 'axios'
import { useState } from 'react'
import { useEffect } from 'react'
import BookForm from './BookForm';
import { Book } from '../../../lib/types'
import BookList from './BookList';

const Books = () => {

    const [books, setBooks] = useState([])
    const [updateBooks, setUpdateBooks] = useState(false)
    const emptyBook = {
        id: null,
        title: "",
        author: "",
        description: ""
    }
    const [selectedBook, setSelectedBook] = useState<Book>(emptyBook)

    useEffect(() => {
        axios.get('/books/all')
            .then((response) => {
                setBooks(response.data)
            })
            .catch(() => {
                alert("Oops! Something went wrong. Refresh page or try again later.")
            })
    }, [updateBooks])

    return (
        <div className={styles.main}>
            <BookForm
                selectedBook={selectedBook}
                setSelectedBookEmpty={() => { setSelectedBook(emptyBook) }}
                updateBooksList={() => { setUpdateBooks(!updateBooks); }}
            />
            <BookList
                selectedBook={selectedBook}
                setSelectedBook={setSelectedBook}
                setSelectedBookEmpty={() => { setSelectedBook(emptyBook) }}
                books={books}
            />
        </div>
    )
}

export default Books