import styles from './App.module.css';
import Books from './Components/Books/Books';

const App = () => {
  return (
    <div className={styles.main}>
      <Books />
    </div>
  )
}

export default App;
