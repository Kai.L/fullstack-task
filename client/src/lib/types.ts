
export type Book = {
    id: number | null;
    title: string;
    author: string;
    description: string;
}