import styles from './Button.module.css'

interface Props {
    label: string;
    onClickFunction: Function;
    disabled: boolean;
    color: string;
}

const Button = ({
    label,
    onClickFunction,
    disabled,
    color,
}: Props): JSX.Element => {
    return (
        <button
            className={styles.main}
            onClick={() => { onClickFunction() }}
            disabled={disabled}
            style={{ backgroundColor: color }}
        >
            {label}
        </button>
    )
}

export default Button