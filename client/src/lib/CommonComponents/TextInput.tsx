import styles from './TextInput.module.css'

interface Props {
    value: string;
    setValue: (value: string) => void
    placeHolder: string;
    width: string;
    padding: string;
    margin: string;
    area?: boolean;
    label: string;
    height?: string;
}

const TextInput = ({
    value,
    setValue,
    placeHolder,
    width,
    padding,
    margin,
    area,
    label,
    height
}: Props) => {
    return (
        <div className={styles.main}>
            <label
                className={styles.label}
                style={{ margin }}
            >
                {label}
            </label>
            {area ?
                <textarea
                    className={styles.inputField}
                    style={{
                        resize: 'vertical',
                        width,
                        padding,
                        margin,
                        height
                    }}
                    value={value}
                    onChange={(event) => { setValue(event.target.value) }}
                    placeholder={placeHolder}
                />
                :
                <input
                    className={styles.inputField}
                    style={{
                        width,
                        padding,
                        margin,
                        height
                    }}
                    type={"text"}
                    value={value}
                    onChange={(event) => { setValue(event.target.value) }}
                    placeholder={placeHolder}
                />
            }
        </div>
    )
}

export default TextInput